## v0.0.1

- AT3-320 Create ska-pst-buildtools docker image
- AT3-322 Develop downstream pipeline triggers between ska-pst-common and ska-pst-smrb
- AT3-338 Configure spdlog to output the required SKAO logging format in PST CORE
- AT3-345 Ensure SPDLOG builds with position independent code and constraint linting false positives
- AT3-377 Fix gitlab configuration information

## v0.0.2

- AT3-349 Update psrdada dependency to acquire shm destruction bug fix

## v0.0.3 

- AT3-490 Upgrade Ubuntu base image from 20.04 to 22.04, APT package lists and pipeline machinery

## v0.0.4

- AT3-591 Update ska-cicd-makefile and fix 'make make'
- AT3-598 Refactor for the ska-pst mono repository, update buildtools with libpcap dependencies

## v0.0.5 (unreleased)

- AT3-616 bump patch release to ensure libpcap present in images

## v0.0.6 (unreleased)

- AT3-625 Add CUDA support into build-tools

## v0.0.7 (unreleased)

- AT3-648 fix spdlog nvcc build issues, switch to multi-stage builds

## v0.0.8

- AT3-677 migrate to cuda-11.8.0 with GPU architecture support of 3.5,7.0,8.0,8.9 
- AT3-728 update .make .pst and psrdada dependencies

## v0.0.9

- AT3-756 update .make .pst and psrdada dependencies

## v0.0.10

- AT3-812 update gRPC, spdlog, protoc-doc-gen and added uuid-dev/runtime dependencies

## v0.0.11

- AT3-871 update psrdada to fix bug with zero byte observations, update gRPC from 1.65.0 to 1.68.2, update SPDLOG from 1.14.1 to 1.50.0

